# Video Games Preservation Wiki

Hi, welcome to the VGPW (Video Games Preservation Wiki).

In this place, you'll find help and some definitions on what is video game preservation, and how to help for it.

## Links

Here are some links you can follow to browse on the website:

- [Retro meaning](retro-meaning.md)
- [What to preserve](what-to-preserve.md)
- [How to preserve](how-to-preserve.md)
- [Keep collection up-to-date](keep-collection-uptodate.md)